#   Conception et implémentation d'un dispositif à retour de force 1DOF

##  Présentation générale

Sujet pas encore affecté.

### Résumé

Les dispositifs à retour de force permettent de manipuler des objets virtuels, avec la sensation de les toucher. Le calcul de simulations précises nécessitent des calculs complexes à haute fréquence. Ceci est en général effectué par des ordinateurs externes puissants, ce qui est un frein à l'utilisation du retour de force dans des dispositifs portables comme des instruments de musique digitaux. Ces systèmes utilisent deux boucles de calcul : une à haute fréquence (1000Hz) calculant la force à appliquer, et une à plus basse fréquence (100Hz) qui définit les modèles de calcul à utiliser. Nous proposons une nouvelle approche, dans laquelle la boucle à haute fréquence est intégrée au dispositif à retour de force, et l'ordinateur hôte se contentera de gérer les modèles de force. La question à étudier est la limite de complexité de la scène pouvant être représentée. Cette limite pourra être repoussée par la simplification des modèles. Il se pose alors une nouvelle question, celle de la qualité du rendu.

### Mots-clés

Interaction Homme-Machine

### Encadrement

Équipe(s) : Loki

Encadrant(s) :

-   Thomas Pietrzak
-   Marcelo Wanderley (McGill University, Montréal, Canada)

[Contacter les encadrants](mailto:thomas.pietrzak@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille – Nord Europe, bâtiment B ou McGill University

##  Présentation détaillée

### Pré-requis

Le candidat idéal est étudiant en master en informatique, et montre un grand intérêt pour la recherche en Interaction Homme-Machine. Il doit avoir de l'expérience ou un intérêt fort pour le développement logiciel et matériel. La créativité, indépendance, travail en équipe et des facultés de communication sont des avantages.

### Description

Les dispositifs à retour de force permettent de manipuler des objets virtuels, avec la sensation de les toucher. Le calcul de simulations précises nécessitent des calculs complexes à haute fréquence. Ceci est en général effectué par des ordinateurs externes puissants, ce qui est un frein à l'utilisation du retour de force dans des dispositifs portables comme des instruments de musique digitaux [franco2016]. De plus ceci impose de fortes contraintes temporelles à ces machines, qui ne sont pas conçues pour ça. Ces systèmes utilisent deux boucles de calcul : une à haute fréquence (1000Hz) calculant la force à appliquer, et une à plus basse fréquence (100Hz) qui définit les modèles de calcul à utiliser. Nous proposons une nouvelle approche, dans laquelle la boucle à haute fréquence est intégrée au dispositif à retour de force, et l'ordinateur hôte se contentera de gérer les modèles de force. Ce dernier pourra avoir une puissance moindre, un Raspberry Pi par exemple. La question à étudier est la limite de complexité de la scène pouvant être représentée. Cette limite pourra être repoussée par la simplification des modèles. Il se pose alors une nouvelle question, celle de la qualité du rendu.

Dans ce stage il s'agira d'explorer ce concept au travers de l'implémentation d'un slider motorisé à un degré de liberté, de manière similaire au Firefader [berdahl2013]. Deux approches seront comparées pour le calcul de force : soit par un microcontroleur 32 bits, soit par un FPGA [ishii2007]. L'étude consistera à déterminer les avantages et inconvénients de chaque méthode, étudier la flexibilité du rendu de la scène, ainsi qu'évaluer la qualité du rendu.

### Bibliographie

* Berdahl, Edgard, Kontogeorgakopoulos, A. The FireFader: Simple, open-source, and reconfigurable haptic force feedback for musicians. Computer Music Journal. 37, 1 (2013), 23–34.
* Franco, Ivan, Wanderley, M. The Mitt: Case study in the design of a self-contained digital music instrument. Proceedings of the international symposium on computer music multidisciplinary research (CMMR).
* Ishii, E., Nishi, H., Ohnishi, K. Improvement of performances in bilateral teleoperation by using FPGA. IEEE Trans- actions on Industrial Electronics. 54, 4 (Aug. 2007), 1876–1884.

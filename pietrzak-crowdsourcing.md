#   Étude sur les notions de précision et certitude pour le crowdsourcing

##  Présentation générale

Sujet pas encore affecté.

### Résumé

L'analyse des réponses à des questionnaires de crowdsourcing nécessite de prendre en compte deux aspects importants : la précision de la réponse, et la certitude de l'utilisateur dans sa réponse. Le but des études de crowdsourcing est d'avoir des réponses aussi précises que possible, tout en maintenant un niveau de certitude suffisamment élevé.

Dans ce stage, il s'agit d'étudier l'influence de la façon de poser et de répondre à une question sur la précision et la certitude. Pour cela nous allons mener des études sur lesquelles nous contrôlerons la certitude par le biais de la difficulté de la tâche. L'objectif du stage sera donc dans un premier temps de concevoir un protocole d'étude permettant de caractériser les liens éventuels entre la difficulté et la certitude afin de mieux comprendre la façon dont l’utilisateur exprime sa propre certitude.

### Mots-clés

Interaction Homme-Machine, crowdsourcing

### Encadrement

Équipe(s) : Loki

Encadrant(s) :

-   Thomas Pietrzak
-   Sylvain Malacria
-   Stéphane Huot

[Contacter les encadrants](mailto:thomas.pietrzak@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille – Nord Europe, bâtiment B


##  Présentation détaillée

### Pré-requis

Le candidat idéal est étudiant en master en informatique, et montre un grand intérêt pour la recherche en Interaction Homme-Machine. Il doit avoir de l'expérience ou un intérêt fort pour le développement logiciel, si possible en Node.js. La créativité, indépendance, travail en équipe et des facultés de communication sont des avantages.

### Description

L'analyse des réponses à des questionnaires de crowdsourcing nécessite de prendre en compte deux aspects importants : la précision de la réponse, et la certitude de l'utilisateur dans sa réponse. La précision se réfère à l'étendue d'une réponse. Sur une question à choix discret cela correspond au nombre de réponses sélectionnées, alors que sur une question à choix continu cela correspond à la largeur de l'intervalle sélectionné. La certitude correspond à la confiance de l'utilisateur que la bonne réponse soit contenue dans la sienne. Le but des études de crowdsourcing est d'avoir des réponses aussi précises que possible, tout en maintenant un niveau de certitude suffisamment élevé.

Dans ce stage, il s'agit d'étudier l'influence de la façon de poser et de répondre à une question sur la précision et la certitude. Pour cela nous allons mener des études sur lesquelles nous contrôlerons la certitude par le biais de la difficulté de la tâche. L'objectif du stage sera donc dans un premier temps de concevoir un protocole d'étude permettant de caractériser les liens éventuels entre la difficulté et la certitude afin de mieux comprendre la façon dont l’utilisateur exprime sa propre certitude.
